# [1.1.0](https://gitlab.com/vardhan27/testrepo/compare/v1.0.2...v1.1.0) (2020-11-23)


### Features

* new message ([0974b50](https://gitlab.com/vardhan27/testrepo/commit/0974b504e7b61ff8e5e249f2174fdd62f07a1a15))

## [1.0.2](https://gitlab.com/vardhan27/testrepo/compare/v1.0.1...v1.0.2) (2020-11-23)


### Bug Fixes

* fixed config name ([f0f5068](https://gitlab.com/vardhan27/testrepo/commit/f0f50681e0e9eeb4fc8e32879179ea677551a616))

## [1.0.1](https://gitlab.com/vardhan27/testrepo/compare/v1.0.0...v1.0.1) (2020-11-23)


### Bug Fixes

* removed a script ([daab324](https://gitlab.com/vardhan27/testrepo/commit/daab3242d45de4b849098e026e605400704ad1c8))

# 1.0.0 (2020-11-23)


### Features

* initial commit ([3028d4a](https://gitlab.com/vardhan27/testrepo/commit/3028d4adec075c64d02c9f95d1d25fac5c6918bf))
