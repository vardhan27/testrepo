module.exports = {
    "verifyConditions": [],
    plugins: [
        [
            "@semantic-release/commit-analyzer",
            {
                preset: "conventionalcommits",
            },
        ],
        '@semantic-release/release-notes-generator',
        ['@semantic-release/changelog',
            {
                changelogFile: "docs/CHANGELOG.md",
            }
        ],
        "@semantic-release/npm",
        [
            "@semantic-release/git",
            {
                assets: ["docs", "dist/**/*.{js,css}"],
            },
        ],
        '@semantic-release/gitlab'
    ],
    branches: [
        'master',
        'dev'
    ],
    dryRun: false
}
